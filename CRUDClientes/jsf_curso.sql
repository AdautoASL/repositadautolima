# Host: localhost  (Version: 5.5.44)
# Date: 2015-08-08 15:26:57
# Generator: MySQL-Front 5.3  (Build 4.191)

/*!40101 SET NAMES latin1 */;

#
# Structure for table "clientes"
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `cliente_codigo` int(11) NOT NULL AUTO_INCREMENT,
  `endereco_codigo` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `idade` int(11) DEFAULT NULL,
  PRIMARY KEY (`cliente_codigo`),
  UNIQUE KEY `id` (`cliente_codigo`),
  KEY `endereco` (`endereco_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "clientes"
#

INSERT INTO `clientes` VALUES (1,0,'Adauto da Silva Lima','adautoddasilvalima@gmail.com',22),(2,0,'Aldo Guimar�es Rodsa','sememail.com.br',101),(4,0,'asasawww','asasa@gmail.com',22),(5,0,'zxzxzx','zxzxx@gmail.com',5);

#
# Structure for table "endereco"
#

DROP TABLE IF EXISTS `endereco`;
CREATE TABLE `endereco` (
  `endereco_codigo` int(11) NOT NULL DEFAULT '0',
  `uf_codigo` tinyint(4) NOT NULL DEFAULT '0',
  `bairro` varchar(70) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `logradouro` varchar(72) DEFAULT NULL,
  `complemento` varchar(72) DEFAULT NULL,
  PRIMARY KEY (`endereco_codigo`),
  KEY `uf` (`uf_codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "endereco"
#

/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;

#
# Structure for table "uf"
#

DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `uf_codigo` tinyint(3) NOT NULL DEFAULT '0',
  `sigla` char(2) DEFAULT NULL,
  `descricao` varchar(72) DEFAULT NULL,
  PRIMARY KEY (`uf_codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Data for table "uf"
#

/*!40000 ALTER TABLE `uf` DISABLE KEYS */;
/*!40000 ALTER TABLE `uf` ENABLE KEYS */;

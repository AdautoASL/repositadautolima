DROP TABLE cliente;
CREATE TABLE cliente (
  id int NOT NULL,
  endereco_id int NOT NULL,
  idade int DEFAULT NULL,
  nome varchar(200) DEFAULT NULL,
  email varchar(200) DEFAULT NULL,
  PRIMARY KEY (id)
);
--
-- Data for table "cliente"
--
INSERT INTO cliente VALUES (1,1,1,'Cliente 1','cliente1@teste.com'),(2,2,2,'Cliente 2','cliente2@teste.com'),(3,3,3,'Cliente 3','cliente3@teste.com'),(4,4,4,'Cliente 4','cliente4@teste.com'),(5,5,5,'Cliente 5','cliente5@teste.com'),(6,6,6,'Cliente 6','cliente6@teste.com'),(7,7,7,'Cliente 7','cliente7@teste.com'),(8,8,8,'Cliente 8','cliente8@teste.com'),(9,9,9,'Cliente 9','cliente9@teste.com'),(10,10,10,'Cliente10','cliente10@teste.com');
--
-- Shown data for table 'cliente'
--
SELECT * FROM APP.cliente;
--
-- Structure for table "endereco"
--
DROP TABLE endereco;
CREATE TABLE endereco (
  id int NOT NULL,
  uf_id char(2) NOT NULL,
  bairro varchar(70) DEFAULT NULL,
  cep varchar(9) DEFAULT NULL,
  logradouro varchar(72) DEFAULT NULL,
  complemento varchar(72) DEFAULT NULL,
  PRIMARY KEY (id)
);
--
-- Data for table "endereco"
--
INSERT INTO endereco VALUES (1,'SP','bairro','12345678','logradouro','complemento'),(2,'SP','bairro','12345678','logradouro','complemento'),(3,'SP','bairro','12345678','logradouro','complemento'),(4,'SP','bairro','12345678','logradouro','complemento'),(5,'SP','bairro','12345678','logradouro','complemento'),(6,'SP','bairro','12345678','logradouro','complemento'),(7,'SP','bairro','12345678','logradouro','complemento'),(8,'SP','bairro','12345678','logradouro','complemento'),(9,'SP','bairro','12345678','logradouro','complemento'),(10,'SP','bairro','12345678','logradouro','complemento');
--
-- Shown data for table 'endereco'
--
SELECT * FROM APP.endereco;
--
-- Structure for table 'uf'
--
DROP TABLE uf;
CREATE TABLE uf (
  sigla char(2) DEFAULT NULL,
  nome varchar(72) DEFAULT NULL,
  PRIMARY KEY (sigla)
);
--
-- Data for table 'uf'
--
INSERT INTO uf VALUES ('SP','São Paulo'),('MG','Minas Gerais');
--
-- Shown data for table 'uf'
--
SELECT * FROM APP.uf;
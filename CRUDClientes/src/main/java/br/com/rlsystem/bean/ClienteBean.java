package br.com.rlsystem.bean;

import br.com.rlsystem.dao.ClienteDAO;
import br.com.rlsystem.vo.ClienteVO;
import java.sql.SQLException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

@ManagedBean(name = "cliBean")
@SessionScoped
public class ClienteBean {

    private ClienteVO clienteVO = new ClienteVO();
    private DataModel<ClienteVO> clientes;

    public String apagar() {
        String retorno = "erro";
        try {
            ClienteDAO dao = new ClienteDAO();
            if (dao.delete(clienteVO)) {
                retorno = "listar";
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("\n\n\napagar()\ngerou o erro:\n" + e);
        }
        return retorno;
    }

    public void novoReg() {
        this.clienteVO = new ClienteVO();
    }

    public void selecionarReg() {
        this.clienteVO = clientes.getRowData();
    }

    public String update() {
        String retorno = "erro";
        try {
            ClienteDAO dao = new ClienteDAO();
            if (dao.update(clienteVO)) {
                retorno = "listar";
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("\n\n\nupdate()\ngerou o erro:\n" + e);
        }
        return retorno;
    }

    public DataModel<ClienteVO> getClientes() {
        ClienteDAO dao = new ClienteDAO();
        try {
            List<ClienteVO> lista = dao.getAll();
            clientes = new ListDataModel<ClienteVO>(lista);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("\n\n\ngetClientes()\ngerou o erro:\n" + e);
        }
        return clientes;
    }

    public void setClientes(DataModel<ClienteVO> clientes) {
        this.clientes = clientes;
    }

    public ClienteVO getClienteVO() {
        return clienteVO;
    }

    public void setClienteVO(ClienteVO clienteVO) {
        this.clienteVO = clienteVO;
    }

    public String addUSer() {
        String retorno = "erro";
        try {
            ClienteDAO dao = new ClienteDAO();
            if (dao.insert(clienteVO)) {
                retorno = "listar";
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("\n\n\naddUSer()\ngerou o erro:\n" + e);
        }

        return retorno;
    }
}

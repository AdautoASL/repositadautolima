package br.com.rlsystem.vo;

public class ClienteVO {

    private int id;
    private String nome;
    private String email;
    private Integer idade;
    private Endereco endereco;

    public ClienteVO() {
    }

    public ClienteVO(String nome, String email, Integer idade, Endereco endereco) {
        this.nome = nome;
        this.email = email;
        this.idade = idade;
        this.endereco = endereco;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteVO other = (ClienteVO) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "ClienteVO [id=" + id + ", " + (nome != null ? "nome=" + nome + ", " : "")
                + (email != null ? "email=" + email + ", " : "") + (idade != null ? "idade=" + idade + ", " : "")
                + (endereco != null ? "endereco=" + endereco + ", " : "") + "getId()=" + getId() + ", "
                + (getNome() != null ? "getNome()=" + getNome() + ", " : "")
                + (getEmail() != null ? "getEmail()=" + getEmail() + ", " : "")
                + (getIdade() != null ? "getIdade()=" + getIdade() + ", " : "")
                + (getEndereco() != null ? "getEndereco()=" + getEndereco() + ", " : "")
                + (getClass() != null ? "getClass()=" + getClass() + ", " : "") + "hashCode()=" + hashCode() + ", "
                + (super.toString() != null ? "toString()=" + super.toString() : "") + "]";
    }

}

package br.com.rlsystem.vo;

public class Endereco {

    private Integer id;
    private Estado uf;
    private String bairro;
    private String cep;
    private String logradouro;
    private String complemento;

    public Endereco() {
    }

    public Endereco(Estado uf, String bairro, String cep, String logradouro, String complemento) {
        this.uf = uf;
        this.bairro = bairro;
        this.cep = cep;
        this.logradouro = logradouro;
        this.complemento = complemento;
    }

    public Estado getUf() {
        return uf;
    }

    public void setUf(Estado uf) {
        this.uf = uf;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Endereco other = (Endereco) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Endereco [id=" + id + ", uf=" + uf + ", bairro=" + bairro + ", cep=" + cep + ", logradouro="
                + logradouro + ", complemento=" + complemento + ", getUf()=" + getUf() + ", getId()=" + getId()
                + ", getBairro()=" + getBairro() + ", getCep()=" + getCep() + ", getLogradouro()=" + getLogradouro()
                + ", getComplemento()=" + getComplemento() + ", hashCode()=" + hashCode() + ", getClass()=" + getClass()
                + ", toString()=" + super.toString() + "]";
    }

}

package br.com.rlsystem.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.rlsystem.vo.ClienteVO;
import br.com.rlsystem.vo.Endereco;
import br.com.rlsystem.vo.Estado;

public class ClienteDAO {

	private Connection getConn() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		return (Connection) DriverManager.getConnection("jdbc:mysql://localhost/jsf_curso","root","root");
	}
	
	public boolean insert(ClienteVO vo) throws SQLException, ClassNotFoundException {
		String SQL = "INSERT INTO clientes (nome, email, idade, endereco_codigo)";
		       SQL += " VALUES (?, ?, ?, ?)";
		       
		PreparedStatement pstm = getConn().prepareStatement(SQL);
		
		pstm.setString(1,vo.getNome());
		pstm.setString(2,vo.getEmail());
		pstm.setInt(3,vo.getIdade());
		pstm.setInt(4,vo.getEndereco().getId());
		
		if (pstm.executeUpdate() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean delete(ClienteVO vo) throws SQLException, ClassNotFoundException {
		String SQL = "DELETE FROM clientes WHERE cliente_codigo = ?";
		       
		PreparedStatement pstm = getConn().prepareStatement(SQL);
		
		pstm.setInt(1,vo.getId());
		
		if (pstm.executeUpdate() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean update(ClienteVO vo) throws SQLException, ClassNotFoundException {
		String SQL = "UPDATE clientes SET nome = ?, email = ?, idade = ?, endereco_codigo = ?";
		       SQL += " WHERE cliente_codigo = ?";
		       
		PreparedStatement pstm = getConn().prepareStatement(SQL);
		
		pstm.setString(1,vo.getNome());
		pstm.setString(2,vo.getEmail());
		pstm.setInt(3,vo.getIdade());
		pstm.setInt(4,vo.getEndereco().getId());
		pstm.setInt(5,vo.getId());
		
		if (pstm.executeUpdate() > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public ClienteVO getById(int ID) throws SQLException, ClassNotFoundException {
		String SQL = "select c.cliente_codigo, c.nome, c.email, c.idade, " +
                             "e.endereco_codigo, e.bairro, e.logradouro, e.complemento, e.cep," +
			     "uf.uf_codigo, uf.sigla, uf.descricao" +
                             "from clientes c " +
			     "left join endereco e using ( endereco_codigo) "+
                             "left join uf using (uf_codigo) WHERE c.cliente_codigo = ?";
		       
		PreparedStatement pstm = getConn().prepareStatement(SQL);
		
		pstm.setInt(1,ID);
		
		ResultSet rs = pstm.executeQuery();
		
		ClienteVO vo = new ClienteVO();
		
		if (rs.first()){
			vo.setId(rs.getInt("cliente_codigo"));
			vo.setNome(rs.getString("nome"));
			vo.setEmail(rs.getString("email"));
			vo.setIdade(rs.getInt("idade"));
			Endereco endereco = new Endereco();
			endereco.setId(rs.getInt("endereco_codigo"));
			endereco.setLogradouro(rs.getString("logradouro"));
			endereco.setComplemento(rs.getString("complemento"));
			endereco.setBairro(rs.getString("bairro"));
			endereco.setCep(rs.getString("cep"));
			Estado uf = new Estado();
			uf.setId(rs.getInt("uf_codigo"));
			uf.setSigla(rs.getString("sigla"));
			uf.setDescricao(rs.getString("descricao"));
			endereco.setUf(uf );
			vo.setEndereco(endereco);			
		}
				
		return vo;
	}
	
	public List<ClienteVO> getAll() throws SQLException, ClassNotFoundException {
		String SQL = "select c.cliente_codigo, c.nome, c.email, c.idade, " +
                             "e.endereco_codigo, e.bairro, e.logradouro, e.complemento, e.cep, " +
                             "uf.uf_codigo, uf.sigla, uf.descricao " +
                             "from clientes c " +
                             "left join endereco e using ( endereco_codigo) "+
                             "left join uf using (uf_codigo)";
		       
		PreparedStatement pstm = getConn().prepareStatement(SQL);
		
		ResultSet rs = pstm.executeQuery();
		
		List<ClienteVO> lista = new ArrayList<ClienteVO>();
		
		
		while (rs.next()){
			ClienteVO vo = new ClienteVO();
			vo.setId(rs.getInt("cliente_codigo"));
			vo.setNome(rs.getString("nome"));
			vo.setEmail(rs.getString("email"));
			vo.setIdade(rs.getInt("idade"));
			
			Endereco endereco = new Endereco();
			endereco.setId(rs.getInt("endereco_codigo"));
			endereco.setLogradouro(rs.getString("logradouro"));
			endereco.setComplemento(rs.getString("complemento"));
			endereco.setBairro(rs.getString("bairro"));
			endereco.setCep(rs.getString("cep"));
			Estado uf = new Estado();
			uf.setId(rs.getInt("uf_codigo"));
			uf.setSigla(rs.getString("sigla"));
			uf.setDescricao(rs.getString("descricao"));
			endereco.setUf(uf );
			vo.setEndereco(endereco);
			
			System.out.println("Teste\n " + vo);
			
			lista.add(vo);
		}
				
		return lista;
	}
}

package br.com.biblioteca.controller;

import br.com.biblioteca.entity.EnderecoPostal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
@Stateless
public class EnderecoPostalFacade extends AbstractFacade<EnderecoPostal> {

    @PersistenceContext(name = "derby_pu")
    private EntityManager em;

    public EnderecoPostalFacade() {
        super(EnderecoPostal.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}

package br.com.biblioteca.controller;

import br.com.biblioteca.entity.Livro;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 * @see <a href=""></a>
 */
@Stateless
public class LivroFacade extends AbstractFacade<Livro> {

    @PersistenceContext(unitName = "derby_pu")
    private EntityManager em;

    public LivroFacade() {
        super(Livro.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}

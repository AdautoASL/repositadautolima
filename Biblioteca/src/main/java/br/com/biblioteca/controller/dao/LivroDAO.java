/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.biblioteca.controller.dao;

import br.com.biblioteca.entity.Livro;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
@Stateless
public class LivroDAO {

    @PersistenceContext
    EntityManager entityManager;

    public void save(Livro livro) {
        System.out.println("save: " + livro);
        entityManager.persist(livro);
    }
}

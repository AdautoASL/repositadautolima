package br.com.biblioteca.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Autor - aquele que produz originalmente uma obra
 *
 * @author tux
 */
@Entity
public class Autor extends BaseEntity {

    private static final long serialVersionUID = -6511811290416948150L;

    /*
     * Cadastro de Pessoas Físicas - CPF
     */
    private String cpf;

    /*
     * Nome do autor
     */
    private String nome;

    /*
     * Endereço completo do autor.
     */
    @OneToOne(cascade = CascadeType.ALL)
    private EnderecoPostal endereco;

    /**
     *
     */
    protected Autor() {
    }

    public Autor(String cpf, String nome, EnderecoPostal endereco) {
        this.cpf = cpf;
        this.nome = nome;
        this.endereco = endereco;
    }

    /**
     * Get the value of endereco
     *
     * @return the value of endereco
     */
    public EnderecoPostal getEndereco() {
        return endereco;
    }

    /**
     * Set the value of endereco
     *
     * @param endereco new value of endereco
     */
    public void setEndereco(EnderecoPostal endereco) {
        this.endereco = endereco;
    }

    /**
     * Get the value of nome
     *
     * @return the value of nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Set the value of nome
     *
     * @param nome new value of nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Get the value of cpf
     *
     * @return the value of cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Set the value of cpf
     *
     * @param cpf new value of cpf
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}

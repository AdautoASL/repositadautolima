package br.com.biblioteca.boundary;

import java.io.Serializable;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

/**
 *
 * @author tux
 */
@Named
@ConversationScoped
public class AutorManagedBean implements Serializable {

    private static final long serialVersionUID = 310407823191195946L;

}

package br.com.biblioteca.boundary;

import java.io.Serializable;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

/**
 *
 * @author tux
 */
@Named
@ConversationScoped
public class EnderecoPostalManagedBean implements Serializable {

    private static final long serialVersionUID = 3914548452554965549L;

}

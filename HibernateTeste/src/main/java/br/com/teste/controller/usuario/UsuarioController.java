/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.controller.usuario;

import br.com.teste.controller.usuario.dao.UsuarioDAO;
import br.com.teste.entity.Usuario;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author tux
 */
@Named
@RequestScoped
public class UsuarioController {

    private Usuario usuario = new Usuario("Fabio", "Almeida", 43, "Jd. Triangulo", "Ferraz de Vasconcelos", "SP", "Brasil");

    public String incluir() {
        System.out.println("Incluir usuario." + usuario);
        new UsuarioDAO().incluir(usuario);
        return "usuarioCadastrado.xhtml";

    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}

package br.com.teste.controller.usuario.dao;

import br.com.teste.JPAUtil;
import br.com.teste.entity.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

/**
 *
 * @author tux
 */
public class UsuarioDAO {

    private Context context;
    private DataSource dataSource;

    public void incluir(Usuario usuario) {

        try {
            context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:comp/env/jdbc/derby");

            try {
                Connection cn = dataSource.getConnection();
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery("SELECT CURRENT_TIMESTAMP FROM SYSIBM.SYSDUMMY1");
                while (rs.next()) {
                    usuario.setPais(rs.getTimestamp(1).toString());
                }
            } catch (SQLException e) {
                //throw new ServletException(e);
                System.out.println("O processo de SELECT gerou um erro!\n" + e);
            }

        } catch (NamingException e) {
            //throw new ServletException(e);
            System.out.println("O processo de lookup gerou um erro!\n" + e);
        }

    }

    public void persist() {
        Usuario usuario = new Usuario("Adauto", "Silva", 25, "Vila Domitila", "São Paulo", "SP", "Brasil");
        EntityManager em = JPAUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(usuario);
            em.getTransaction().commit();
        } catch (Exception e) {
            if (em.isOpen()) {
                em.getTransaction().rollback();
            }
        } finally {
            if (em.isOpen()) {
                em.close();
            }
        }
    }
}

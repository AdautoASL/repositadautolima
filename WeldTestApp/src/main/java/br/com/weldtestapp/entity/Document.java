package br.com.weldtestapp.entity;

/**
 *
 * @author tux
 */
public class Document {

    private User createdBy;

    /**
     * Get the value of createdBy
     *
     * @return the value of createdBy
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * Set the value of createdBy
     *
     * @param createdBy new value of createdBy
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.weldtestapp.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tux
 */
@Entity
public class User implements Serializable {

    private static final long serialVersionUID = -8251462268006431117L;

    public User() {
    }

    private @NotNull
    @Size(min = 3, max = 25)
    @Id
    String username;
    private @NotNull
    @Size(min = 6, max = 20)
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.weldtestapp.managedbean.login;

import br.com.weldtestapp.qualifier.UserDatabase;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tux
 */
class UserDatabaseProducer {

    @Produces
    @UserDatabase
    @PersistenceContext
    static EntityManager userDatabase;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.weldtestapp.managedbean.login;

import br.com.weldtestapp.entity.User;
import br.com.weldtestapp.qualifier.LoggedIn;
import br.com.weldtestapp.qualifier.UserDatabase;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 *
 * @author tux
 */
@SessionScoped
@Named
public class Login implements Serializable {

    private static final long serialVersionUID = -8168444734928345546L;

    @Inject
    Credentials credentials;
    @Inject
    @UserDatabase
    EntityManager userDatabase;
    private User user;

    public void login() {
        List<User> results;
        results = userDatabase.createQuery("select u from User u where u.username = :username and u.password= :password")
                .setParameter("username", credentials.getUsername())
                .setParameter("password", credentials.getPassword())
                .getResultList();

        if (!results.isEmpty()) {
            user = results.get(0);
        } else {
            // perhaps add code here to report a failed login
            System.out.println("Usuário ou senha incorreta!");
        }
    }

    public void logout() {
        user = null;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    @Produces
    @LoggedIn
    User getCurrentUser() {
        return user;
    }
}

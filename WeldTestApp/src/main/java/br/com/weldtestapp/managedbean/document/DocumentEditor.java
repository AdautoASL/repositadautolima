/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.weldtestapp.managedbean.document;

import br.com.weldtestapp.entity.Document;
import br.com.weldtestapp.entity.User;
import br.com.weldtestapp.qualifier.LoggedIn;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author tux
 */
public class DocumentEditor {

    @Inject
    Document document;
    @Inject
    @LoggedIn
    User currentUser;
    @Inject
    @DocumentDatabase
    EntityManager docDatabase;

    public void save() {
        document.setCreatedBy(currentUser);
        docDatabase.persist(document);
    }
}

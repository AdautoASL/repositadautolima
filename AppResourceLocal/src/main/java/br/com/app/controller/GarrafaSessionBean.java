package br.com.app.controller;

import br.com.app.entity.Garrafa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author tux
 */
@Stateless
public class GarrafaSessionBean {

    @PersistenceContext
    private EntityManager entityManager;

    public Garrafa getGarrafa(String modelo) {
        Query query = entityManager.createQuery("SELECT g FROM Garrafa WHERE modelo = :modelo");
        query.setParameter("modelo", modelo);
        Garrafa garrafa = null;
        try {
            garrafa = (Garrafa) query.getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        }
        return garrafa;
    }
}
